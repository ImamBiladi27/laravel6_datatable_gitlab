<?php
class TestApplication {


public function run() {

    $shop  = new Shop;


    $books = array(
        new Book('The Fellowship of the Ring', 'J.R.R. Tolkien'),
        new Book('The Two Towers', 'J.R.R. Tolkien'),
        new Book('The Return of the King', 'J.R.R. Tolkien'),
        new Book('The Hobbit', 'J.R.R. Tolkien'),
        new Book('Harry Potter and the Sorcerer\'s Stone', 'J.K. Rowling'),
        new Book('Harry Potter and the Chamber of Secrets', 'J.K. Rowling'),
        new Book('Harry Potter and the Prisoner of Azkaban', 'J.K. Rowling'),
        new Book('Harry Potter and the Goblet of Fire', 'J.K. Rowling'),
        new Book('Harry Potter and the Order of the Phoenix', 'J.K. Rowling'),
        new Book('Harry Potter and the Half-Blood Prince', 'J.K. Rowling'),
        new Book('Harry Potter and the Deathly Hallows', 'J.K. Rowling'),
    );


    foreach ($books as $book) {
        $shop->bookAdd($book);
    }



    // Ahmad goes to the bookstore.
    // He looks for a book title "Two Towers"
    // If the book is there, he puts it on book cart.

    $ahmad = new Person('Ahmad Ramadhan');

    $available = $shop->bookIsAvailable('The Two Towers');

    if ($available) {

        $book = $shop->bookGet('The Two Towers');
        $ahmad->addToBag($book);

        echo "1: true";

    } else {

        echo "1: false";
    }

    echo "\n";


    // Ahmad goes to bookcase collection from author J.K. Rowling
    // He looks for a book title "Harry Potter and The Goblet of Fire" from author J.K Rowling
    // If the book is there, he puts it on book cart.


    $rowlingBooks = $shop->bookListByAuthor('J.K. Rowling');

    if (in_array('Harry Potter and the Goblet of Fire', $rowlingBooks)) {

        $book = $shop->bookGet('Harry Potter and the Goblet of Fire');
        $ahmad->addToBag($book);

        echo "2: true";

    } else {

        echo "2: false";
    }

    echo "\n";


    // Ahmad has finished choose the books and goes to cashier.
    // He checked the bookcart and sees that there are 2 books on bookcart

    if ($ahmad->countBag() == 2) {

        echo "3: true";

    } else {

        echo "3: false";
    }

    echo "\n";

    // Ahmad finished buy the books.




    // Bayu goes to the bookstore.
    // He looks for a book that he only remembers part of the title is "The King" in entire bookcase.
    // After he finds 1 book with title matched, he puts in on bookcart.

    $bayu = new Person('Bayu Sakti');

    $books_theking = $shop->bookListByTitleContains('The King');

    if (count($books_theking) > 0) {

        $book = $shop->bookGet($books_theking[0]);
        $bayu->addToBag($book);

        echo "4: true";

    } else {

        echo "4: false";
    }

    echo "\n";


    // Accidentally, He looks new arrival of the newest Harry potter's book series.
    // Then he put it on bookcart and turn book "The Return of The King" back to bookcase.


    $available = $shop->bookIsAvailable('Harry Potter and the Deathly Hallows');

    if ($available) {

        $book = $shop->bookGet('Harry Potter and the Deathly Hallows');
        $bayu->addToBag($book);


        $bayu->removeFromBag('The Return of the King');

    }



    // Bayu has finished choose the books and goes to cashier.
    // He checked the bookcart and sees that there is 1 book on bookcart

    if ($bayu->countBag() == 1) {

        echo "5: true";

    } else {

        echo "5: false";
    }

    echo "\n";

    // Bayu finished buy the books.

}
}


class Shop {

private $books = array(); //menyimpan array book

//menambahkan book ke daftar buku yang tersedia di shop
public function bookAdd($book) {
    $this->books[] = $book;
}

//untuk mengecek apakah buku dengan title tertentu tersedia di shop
public function bookIsAvailable($title) {
    foreach ($this->books as $book) {
        if ($book->getTitle() == $title) { //jika kondisi title memenuhi maka akan nilai dikembalikan dengan benar
            return true; 
        }
    }
    return false;
}
//untuk mengambil book dengan title tertentu dari shop
public function bookGet($title) {
    foreach ($this->books as $key => $book) {
        if ($book->getTitle() == $title) { //jika kondisi title memenuhi akan dihapus dan nilai akan direturn
            unset($this->books[$key]);
            return $book;
        }
    }
    return null;
}
//mengembalikan daftar book dari author
public function bookListByAuthor($author) {
    $authorBooks = array();
    foreach ($this->books as $book) {
        if ($book->getAuthor() == $author) {
            $authorBooks[] = $book->getTitle();
        }
    }
    return $authorBooks;
}
//mengembalikan daftar book yang titlenya mengandung konten tertentu
public function bookListByTitleContains($partialTitle) {
    $matchingBooks = array();
    foreach ($this->books as $book) {
        if (stripos($book->getTitle(), $partialTitle) !== false) {
            $matchingBooks[] = $book->getTitle();
        }
    }
    return $matchingBooks;
}

}


class Book {

    private $title; //menyimpan tittle buku
    private $author; //menyimpan author buku

    //untuk menginisiasi objek book dengan title dan author
    public function __construct($title, $author) {
        $this->title = $title;
        $this->author = $author;
    }

    //untuk mengembalikan nilai dari title
    public function getTitle() {
        return $this->title;
    }
    //untuk mengembalikan author 
    public function getAuthor() {
        return $this->author;
    }

}



class Person {

    private $name; //Digunakan untuk menyimpan name pemilik tas belanja.
    private $bag = array(); //Digunakan untuk menyimpan array buku yang ada di tas belanja.

    //untuk menginisialisasi objek person dengan nama.
    public function __construct($name) {
        $this->name = $name;
    }

    //untuk menambahkan book ke bag belanja
    public function addToBag($book) {
        $this->bag[] = $book;
    }

    //untuk menghapus book dari bag berdasarkan judul
    public function removeFromBag($title) {
        foreach ($this->bag as $key => $book) {
            if ($book->getTitle() == $title) {
                unset($this->bag[$key]);
                break;
            }
        }
    }
    // menghitung jumlah buku di dalam bag
    public function countBag() {
        return count($this->bag);
    }

}
?>