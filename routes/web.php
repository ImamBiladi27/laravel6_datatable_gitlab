<?php

use App\Http\Controllers\Auth\LoginController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/get-api-data', [\App\Http\Controllers\DashboardController::class, 'getApiData'])->name('get.api.data');

Route::get('/', [\App\Http\Controllers\LoginController::class, 'login'])->name('login');
Route::post('actionlogin', [\App\Http\Controllers\LoginController::class, 'actionlogin'])->name('actionlogin');

Route::get('actionlogout', [\App\Http\Controllers\LoginController::class, 'actionlogout'])->name('actionlogout')->middleware('auth');
// Route::resource('/todo', \App\Http\Controllers\DashboardController::class);
Route::post('/insert-student',  [\App\Http\Controllers\DashboardController::class, 'store'])->name('store');
Route::get('/dashboard', [App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard');
Route::get('/kelas', [App\Http\Controllers\DashboardController::class, 'kelas'])->name('kelas');

Route::get('/kelas/getKelas', [App\Http\Controllers\DashboardController::class, 'getKelas'])->name('getKelas');
Route::get('/general_info', [App\Http\Controllers\DashboardController::class, 'general_info']);

Route::get('/dashboard/getData', [App\Http\Controllers\DashboardController::class, 'getData'])->name('getData');
Route::get('/general_info/getDataInfo', [App\Http\Controllers\DashboardController::class, 'getDataInfo'])->name('getDataInfo');

Route::post('/insert-class',  [\App\Http\Controllers\DashboardController::class, 'storeclass'])->name('storeclass');

// Route::post('/delete', [\App\Http\Controllers\DashboardController::class, 'delete'])->name('delete');
// Route::delete('/data/delete', 'DataController@deleteData')->name('data.deleteData');
Route::post('/delete-item',  [\App\Http\Controllers\DashboardController::class, 'deleteItem'])->name('delete.item');
// Route::post('/get-item',  [\App\Http\Controllers\DashboardController::class, 'getItemById'])->name('get.item');
Route::post('/update-item',[\App\Http\Controllers\DashboardController::class, 'updateItem'])->name('update.item');

Route::get('/data/{id}/edit', [\App\Http\Controllers\DashboardController::class, 'edit']);
// Route::put('/data/{id}', [\App\Http\Controllers\DashboardController::class, 'update']);
