<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class student extends Model
{
    //
    public $timestamps = false;
    protected $table = 'student';
    const created_date = 'created_date';
    const modified_date = 'modified_date';
    public $fillable = ['id','username', 'email', 'age','phone_number','picture','created_by','modified_by'];
}
