<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class student_class extends Model
{
    //
    public $timestamps = false;
    protected $table = 'student_class';
    const created_date = 'created_date';
   
    public $fillable = ['id','student_id', 'class_id','created_by'];
}
