<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class classes extends Model
{
    //
    public $timestamps = false;
    protected $table = 'classes';
    const created_date = 'created_date';
    const modified_date = 'modified_date';
    public $fillable = ['id','name', 'major','created_by','modified_by'];

}
