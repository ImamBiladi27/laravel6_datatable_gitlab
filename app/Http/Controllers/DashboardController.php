<?php

namespace App\Http\Controllers;


use App\Admin;
use App\student;
use App\classes;
use App\student_class;
use Illuminate\View\View;
use Illuminate\Support\Facades\DB;
use DataTables;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Yajra\DataTables\Contracts\DataTable;
use Illuminate\Support\Facades\Redirect;
// use Illuminate\Support\Facades\Http;
class DashboardController extends Controller
{
    public function deleteItem(Request $request)
    {
        $itemId = $request->input('item_id');
        $item = student_class::find($itemId);
        if ($item) {

            $item->delete();

            return response()->json(['success' => true]);
        }

        return response()->json(['success' => false, 'message' => 'Item not found.']);
    }

    public function index(): View
    {

        try {
            $selectOptions = Classes::pluck('major', 'id');
        } catch (\Exception $e) {
            if ($this->isIdAlreadySelected($selectOptions)) {
                return redirect('/form')->with('error', 'ID sudah dipilih sebelumnya');
            }
            // Tangani kesalahan ketika gagal mengambil data dari database
            Session::flash('error', 'Gagal mengambil data.');
            return redirect('/dashboard')->withErrors(['error' => 'Gagal mengambil data.']);
        }
        return view('dashboard', compact('selectOptions'));
    }
    public function general_info(): View
    {
        try {
            $selectOptions = classes::pluck('id');
        } catch (\Exception $e) {
            // Tangani kesalahan ketika gagal mengambil data dari database
            Session::flash('error', 'Gagal mengambil data.');
            return redirect('/dashboard')->withErrors(['error' => 'Gagal mengambil data.']);
        }
        return view('general_info');
    }

    public function getData()
    {
        $data = student_class::select('student_class.id', 'student.username as username', 'student.email as email', 'student.age as age', 'student.phone_number as phone_number', 'classes.name as name', 'classes.major as major')
            ->join('student', 'student_class.student_id', '=', 'student.id')
            ->join('classes', 'student_class.class_id', '=', 'classes.id')
            ->get();
        return DataTables::of($data)
            ->addColumn('action', function ($row) {
                return '<button class="btn btn-danger" onclick="deleteItem(' . $row->id .  ')">Delete</button>
              
                <button type="button" class="btn btn-primary" onclick="openEditModal(' . $row->id .  ')">Edit Data</button>
                ';
            })
            ->addColumn('student_class.id', 'student.username as username', 'student.email as email', 'student.age as age', 'student.phone_number as phone_number', 'classes.name as name', 'classes.major as major', function ($row) {
                return $row->related_column;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function getKelas()
    {
        $data = classes::select('classes.id', 'classes.name', 'classes.major')->get();
        return DataTables::of($data)->make(true);
    }
    public function store(Request $request)
    {

        $student = student::create([
            'id' => $request->input('id_student'),
            'username' => $request->input('username'),
            'email' => $request->input('email'),
            'age' => $request->input('age'),
            'phone_number' => $request->input('phone_number'),
            'picture' => $request->input('picture'),
            'created_by' => $request->input('created_by'),
            'modified_by' => $request->input('modified_by'),
        ]);
        $student_class = student_class::create([
            'student_id' => $request->input('id_student'),
            'class_id' => $request->input('id_class'),
            'created_by' => $request->input('created_by'),
        ]);


        return redirect('/dashboard');
    }
    public function storeclass(Request $request)
    {

        $classes = classes::create([
            'id' => $request->input('id_class'),
            'major' => $request->input('major'),
            'name' => $request->input('name'),
            'created_by' => $request->input('created_by'),
            'modified_by' => $request->input('modified_by'),
        ]);
        return redirect('/kelas');
    }

    public function kelas()
    {
        $client = new Client();

        $response = $client->get('https://www.eannov8.com/career/case/getMajor.json');

        $data = json_decode($response->getBody(), true);
        return view('kelas', ['data' => $data]);
    }

    // public function edit($id){
    //     $student = student::find($id);

    //     return view('dashboard', compact('student'));
    // }

    // public function update(Request $request, $id)
    // {
    //     // Validasi data jika diperlukan
    //     // $this->validate($request, [
    //     //     // Atur aturan validasi sesuai kebutuhan Anda
    //     // ]);

    //     $data = student_class::find($id);
    //     $data->fill($request->all());
    //     $data->save();

    //     return response()->json(['success' => true]);
    // }
    
    public function updateItem(Request $request)
    {
        $id = $request->input('id');

        $data = student_class::find($id);

        if (!$data) {
            return response()->json(['error' => 'Record not found'], 404);
        }

        // Update the fields based on your form structure
        $data->student_id = $request->input('student_id');
        $data->class_id = $request->input('class_id');
        // Update other fields as needed
        $data->save();

        // Assuming you want to update related Student and Class records as well
        $student = Student::find($data->student_id);
        $student->username = $request->input('username');
        $student->email = $request->input('email');
        $student->age = $request->input('age');
        $student->phone_number = $request->input('phone_number');
        $student->save();

        $class = Classes::find($data->class_id);
        $class->name = $request->input('name');
      
        $class->save();

        return Redirect::route('dashboard')->with('success', 'Record updated successfully');
    }
    public function edit($id)
    {
    
        $data = student_class::select('student_class.id', 'student.username as username', 'student.id as student_id','student.email as email', 'student.age as age', 'student.phone_number as phone_number','classes.id as class_id', 'classes.name as name', 'classes.major as major')
            ->join('student', 'student_class.student_id', '=', 'student.id')
            ->join('classes', 'student_class.class_id', '=', 'classes.id')
            ->find($id);
        return response()->json($data);
    }

    // public function getDataInfo()
    // {
    //     $data = student_class::select('student_class.id', 'student.username as username', 'student.email as email', 'student.age as age', 'student.phone_number as phone_number', 'classes.name as name', 'classes.major as major')
    //         ->join('student', 'student_class.student_id', '=', 'student.id')
    //         ->join('classes', 'student_class.class_id', '=', 'classes.id')
    //         ->get();
    //     return DataTables::of($data)
    //         ->make(true);
    // }

}
