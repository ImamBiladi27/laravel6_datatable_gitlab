-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               8.0.30 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             12.1.0.6537
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for laravel6
CREATE DATABASE IF NOT EXISTS `laravel` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `laravel`;

-- Dumping structure for table laravel6.admins
CREATE TABLE IF NOT EXISTS `admins` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT '2023-11-26 05:31:33',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table laravel6.admins: ~0 rows (approximately)
REPLACE INTO `admins` (`id`, `username`, `email`, `password`, `created_date`) VALUES
	(1, 'admin', 'admin@gmail.com', '$2y$10$Ka0FsX159PsQJNVUJi7BfuujMOcdGLwycBWC49Ey0.hoSMFtzJ/ja', '2023-11-26 05:31:33');

-- Dumping structure for table laravel6.classes
CREATE TABLE IF NOT EXISTS `classes` (
  `id` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `major` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT '2023-11-26 05:31:33',
  `modified_by` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modified_date` timestamp NOT NULL DEFAULT '2023-11-26 05:31:33',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table laravel6.classes: ~4 rows (approximately)
REPLACE INTO `classes` (`id`, `name`, `major`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
	('1', 'A', 'Pemrograman Web', 'Ahmad', '2023-11-26 05:31:33', 'Ahmad', '2023-11-26 05:31:33'),
	('2', 'B', 'Arsitektur Komputer', 'Arifin', '2023-11-26 05:31:33', 'Arifin', '2023-11-26 05:31:33'),
	('3', 'C', 'Basis Data', 'mahfud', '2023-11-26 05:31:33', 'mahfud', '2023-11-26 05:31:33'),
	('5', 'D', 'Basis Data', 'ma', '2023-11-26 05:31:33', 'ma', '2023-11-26 05:31:33');

-- Dumping structure for table laravel6.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table laravel6.failed_jobs: ~0 rows (approximately)

-- Dumping structure for table laravel6.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table laravel6.migrations: ~0 rows (approximately)
REPLACE INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_08_19_000000_create_failed_jobs_table', 1),
	(4, '2023_11_24_130717_create_admins', 1),
	(5, '2023_11_24_131309_create_Classes', 1),
	(6, '2023_11_24_131410_create_student_class', 1),
	(7, '2023_11_24_131531_create_student', 1);

-- Dumping structure for table laravel6.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table laravel6.password_resets: ~0 rows (approximately)

-- Dumping structure for table laravel6.student
CREATE TABLE IF NOT EXISTS `student` (
  `id` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` int NOT NULL,
  `phone_number` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_by` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT '2023-11-26 05:31:34',
  `modified_by` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modified_date` timestamp NOT NULL DEFAULT '2023-11-26 05:31:34',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table laravel6.student: ~2 rows (approximately)
REPLACE INTO `student` (`id`, `username`, `email`, `age`, `phone_number`, `picture`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
	('1', 'ws', 'admin1@gmail.com', 22, '083857120078', NULL, 'www', '2023-11-26 05:31:34', 'ww', '2023-11-26 05:31:34'),
	('6', 'mahmud', 'mahmud@gmail.com', 22, '083857120098', '71b3mq2f6pbb.png', 'mahmud', '2023-11-26 05:31:34', 'mahmud', '2023-11-26 05:31:34');

-- Dumping structure for table laravel6.student_class
CREATE TABLE IF NOT EXISTS `student_class` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `student_id` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class_id` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT '2023-11-26 05:31:34',
  PRIMARY KEY (`id`),
  KEY `student_class_class_id_foreign` (`class_id`),
  KEY `student_id` (`student_id`),
  CONSTRAINT `student_class_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `student_class_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table laravel6.student_class: ~2 rows (approximately)
REPLACE INTO `student_class` (`id`, `student_id`, `class_id`, `created_by`, `created_date`) VALUES
	(19, '1', '1', 'www', '2023-11-26 05:31:34');

-- Dumping structure for table laravel6.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table laravel6.users: ~0 rows (approximately)

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
