<!doctype html>
<html lang="en">
  <head>

    <!-- https://getbootstrap.com/docs/4.0/examples/ -->

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="https://getbootstrap.com/favicon.ico">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="stylesheet" href="https://getbootstrap.com/docs/4.0/examples/dashboard/dashboard.css">

     {{-- <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"> --}}

  <!-- DataTables CSS -->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
  {{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css"> --}}
  {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"> --}}
    <title>Dashboard Template for Bootstrap 4</title>
  </head>
  <body>

    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Company name</a>
      
      <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="{{route('actionlogout')}}">Sign out</a>
        </li>
      </ul>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link" href="{{ route('dashboard') }}">
                  <span data-feather="home"></span>
                  Module Student 
                </a>
                
              </li>
              
              <li class="nav-item">
                <a class="nav-link active" href="{{ route('kelas') }}">
                  <span data-feather="file"></span>
                  Module Class  <span class="sr-only">(current)</span>
                </a>
              </li>
             
            <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
              <span>Tab Link</span>
              <a class="d-flex align-items-center text-muted" href="#">
                <span data-feather="plus-circle"></span>
              </a>
            </h6>
            <ul class="nav flex-column mb-2">
              <li class="nav-item">
                <a class="nav-link active" href="#">
                  <span data-feather="home"></span>
                Assign Class <span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link " href="#">
                  <span data-feather="home"></span>
                 General Info 
                </a>
              </li>
              
            </ul>
          </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
          
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tambahDataModalClass">
                Insert Data Class
              </button>
           
          
            <
            <!-- Modal Class -->
            <div class="modal" id="tambahDataModalClass" tabindex="-1" role="dialog" aria-labelledby="tambahDataModalLabelClass" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="tambahDataModalLabel">Insert Class Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <!-- Form untuk menambah data class -->
                    <form method="post" action="{{ url('/insert-class') }}" onsubmit="return validateForm()">
                      {{ csrf_field() }}

                  
                      <div class="form-group">
                        <label for="id_class">ID Class:</label>
                        <input type="number" class="form-control" name="id_class" placeholder="Input ID Class" required>
                      </div>
                      <div class="form-group">
                        <label for="kelas">Jenis Kelas:</label>
                        <input type="text" class="form-control" name="name"  placeholder="Input Jenis Kelas" required>
                      </div>
                      <div class="form-group">
                        <label for="kelas">Major:</label>
                        <select name="major" id="major">
                            
                     
                                <option value="{{  $data['data'][0]['name'] }}">{{  $data['data'][0]['name'] }}</option>
                                <option value="{{  $data['data'][1]['name'] }}">{{  $data['data'][1]['name'] }}</option>
                                <option value="{{  $data['data'][2]['name'] }}">{{  $data['data'][2]['name'] }}</option>
                                <option value="{{  $data['data'][3]['name'] }}">{{  $data['data'][3]['name'] }}</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="created_by">Created By:</label>
                        <input type="text" class="form-control" name="created_by" placeholder="Input Created By" required>
                      </div>
                      <div class="form-group">
                        <label for="modified_by">Modified By:</label>
                        <input type="text" class="form-control" name="modified_by" placeholder="Input Modified By" required>
                      </div>
                      <!-- Tambahkan kolom formulir lainnya sesuai kebutuhan -->
          
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <h2>Class Data</h2>
          <div class="container mt-5">
            <table class="table table-bordered" id="example">
              <thead>
                <tr>
                  <th>ID Class</th>
                  <th>Nama Kelas</th>
                  <th>Major</th>
                  
                </tr>
              </thead>
              
            </table>
          </div>
           
        </main>
      </div>
    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script> --}}
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
      feather.replace()
    </script>

    <!-- Graphs -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
   <!-- jQuery and Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popperjs/core@2.0.7/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>


<!-- Mandatory Form -->
<script>
  function validateForm() {
    var nama = document.getElementById('nama').value;
    var email = document.getElementById('email').value;
    var age = document.getElementById('age').value;
    var phone_number = document.getElementById('phone_number').value;
    var major = document.getElementById('major').value;

    // Simple validation, you can customize this as needed
    if (nama.trim() === '') {
      alert('nama is required!');
      return false;
    }

    if (email.trim() === '') {
      alert('Email is required!');
      return false;
    }
    
    if (age.trim() === '') {
      alert('Age is required!');
      return false;
    }
    
    if (email.trim() === '') {
      alert('Email is required!');
      return false;
    }
    
    if (major.trim() === '') {
      alert('Major is required!');
      return false;
    }

    return true;
  }
</script>



<!-- Datatable -->
<script>
  $(document).ready(function() {
    $('#example').DataTable({
      processing: true,
                serverSide: true,
                ajax: "{{ route('getKelas') }}",
    
      columns: [
                    { data: 'id', name: 'id' },
                   
                    { data: 'name', name: 'name' },
                    { data: 'major', name: 'major' },
            
                       
                ]

    });
  });
  
</script>
<script>
  // Ajax request untuk mengambil data dari API dan menambahkannya ke dalam select box
  var selectBox = document.getElementById('selectData');

  fetch('{{ route("get.api.data") }}')
      .then(response => response.json())
      .then(data => {
          data.forEach(item => {
              var option = document.createElement('option');
              option.value = item.id; // Sesuaikan dengan field yang ingin ditampilkan
              option.text = item.nama; // Sesuaikan dengan field yang ingin ditampilkan
              selectBox.add(option);
          });
      });
</script>

  </body>
</html>