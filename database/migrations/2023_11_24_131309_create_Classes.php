<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClasses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classes', function (Blueprint $table) {
            $table->string('id',64)->nullable(false)->primary();
            $table->string('name',64)->nullable(false);
            $table->string('major',64)->nullable(false);
            $table->string('created_by',64)->nullable(false);
            $table->timestamp('created_date')->default(now())->nullable(false);
            $table->string('modified_by',64)->nullable(false);
            $table->timestamp('modified_date')->default(now())->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classes');
    }
}
