<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdmins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // membuat table admin dengan field seperti dibawah
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id')->length(11);
            $table->string('username',64)->nullable(false);
            $table->string('email',256)->nullable(false);
            $table->string('password')->nullable(false);
            $table->timestamp('created_date')->default(now())->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin');
    }
}
