<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student', function (Blueprint $table) {
            $table->string('id',64)->nullable(false)->primary();
            $table->string('username',256)->nullable(false);
            $table->string('email',256)->nullable(false);
            $table->integer('age')->length(11)->nullable(false);
            $table->string('phone_number',16)->nullable(false);
            $table->text('picture');
            $table->string('created_by',64)->nullable(false);
            $table->timestamp('created_date')->default(now())->nullable(false);
            $table->string('modified_by',64)->nullable(false);
            $table->timestamp('modified_date')->default(now())->nullable(false);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student');
    }
}
