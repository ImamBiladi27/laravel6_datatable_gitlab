<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentClass extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_class', function (Blueprint $table) {
            $table->increments('id')->length(11);
            $table->string('student_id',64)->nullable(false);
            $table->string('class_id',64)->nullable(false);
    
            $table->string('created_by',64)->nullable(false);
            $table->timestamp('created_date')->default(now())->nullable(false);
        
            // $table->foreign('student_id')->references('id')->on('student')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('class_id')->references('id')->on('classes')->onUpdate('cascade')->onDelete('cascade');
            // $table->foreign('student_id')->references('id')->on('student')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_class');
    }
}
